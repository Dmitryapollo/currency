//
//  NetguruCurrencyTests.swift
//  NetguruCurrencyTests
//
//  Created by Дмитрий on 30/10/2020.
//  Copyright © 2020 Дмитрий. All rights reserved.
//

import XCTest
@testable import NetguruCurrency

class NetguruCurrencyTests: XCTestCase {

    var sut: ViewController!
    
    override func setUpWithError() throws {
        sut = ViewController()
        sut.loadViewIfNeeded()
    }
    override func tearDownWithError() throws {
        sut = nil
    }
    
    // MARK: - Setting Table View
    
    func testHasATableView() {
        XCTAssertNotNil(sut.mainTableView)
    }
    
    func testTableViewHasDelegate() {
        XCTAssertNotNil(sut.mainTableView?.delegate)
    }
    
    func testTableViewConfromsToTableViewDelegateProtocol() {
        XCTAssertTrue(sut.conforms(to: UITableViewDelegate.self))
        XCTAssertTrue(sut.responds(to: #selector(sut.tableView(_:didSelectRowAt:))))
        XCTAssertTrue(sut.responds(to: #selector(sut.tableView(_:titleForHeaderInSection:))))
    }
    
    func testTableViewHasDataSource() {
        XCTAssertNotNil(sut.mainTableView?.dataSource)
    }
    
    func testTableViewConformsToTableViewDataSourceProtocol() {
        XCTAssertTrue(sut.conforms(to: UITableViewDataSource.self))
        XCTAssertTrue(sut.responds(to: #selector(sut.tableView(_:numberOfRowsInSection:))))
        XCTAssertTrue(sut.responds(to: #selector(sut.tableView(_:cellForRowAt:))))
    }
    
    func testTableViewConfromsToUITextFieldDelegateProtocol() {
        XCTAssertTrue(sut.conforms(to: UITextFieldDelegate.self))
    }
    
    func testTableViewCellHasReuseIdentifier() {
        let cellFirst = sut.tableView(sut.mainTableView!, cellForRowAt: IndexPath(row: 0, section: 0))
        let actualReuseIdentiferFirstSection = cellFirst.reuseIdentifier
        XCTAssertNotNil(actualReuseIdentiferFirstSection)
    }
    
    func testTableViewFirstSectionHasTitle() {
        let title = sut.tableView(sut.mainTableView!, titleForHeaderInSection: 1)
        XCTAssertEqual(title, "Currency")
    }
    
    // MARK: - Request
    func testDataFetchingURLSession() {
        let configuration = URLSessionConfiguration.default
        let sessionManager = URLSession(configuration: configuration)

        let apiEndPoint = URL(string: "https://api.exchangeratesapi.io/latest?base=PLN")!
        let requestExpectation = expectation(description: "Request should finish")
        sessionManager.dataTask(with: apiEndPoint) { (data, response, error) in
            do {
                if let error = error {
                    throw error
                }
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String : AnyObject]
                if let _ = json?["rates"] as? [String : CGFloat] {
                    requestExpectation.fulfill()
                }
            } catch {
                XCTFail(error.localizedDescription)
            }
        }.resume()

        wait(for: [requestExpectation], timeout: 10.0)
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
