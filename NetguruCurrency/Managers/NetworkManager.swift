//
//  NetworkManager.swift
//  NetguruCurrency
//
//  Created by Дмитрий on 28/10/2020.
//  Copyright © 2020 Дмитрий. All rights reserved.
//

import UIKit

class NetworkManager: NSObject {
    static let sharedInstance = NetworkManager()
    
    func makeRequest(currency: String, completion: @escaping ([String : CGFloat]?, Error?) -> ()) {
        guard let url = URL(string: "https://api.exchangeratesapi.io/latest?base=\(currency)") else { return }

        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if let error = error {
                completion(nil, error)
            }
            
            guard let data = data else { return }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : AnyObject]
                if let dictionary = json?["rates"] as? [String : CGFloat] {
                    completion(dictionary, nil)
                }
            } catch (let error) {
                completion(nil, error)
            }
        }

        task.resume()
    }
}
