//
//  CurrencyModel.swift
//  NetguruCurrency
//
//  Created by Дмитрий on 28/10/2020.
//  Copyright © 2020 Дмитрий. All rights reserved.
//

import UIKit

class CurrencyModel : NSObject {
    let name: String
    var value: CGFloat
    var isSelected: Bool
    
    init(name: String, value: CGFloat, isSelected: Bool) {
        self.name = name
        self.value = value
        self.isSelected = isSelected
    }
}
