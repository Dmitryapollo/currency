//
//  ViewController.swift
//  NetguruCurrency
//
//  Created by Дмитрий on 28/10/2020.
//  Copyright © 2020 Дмитрий. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {

    var mainTableView: UITableView?
    
    private var clearView = UIView()
    var currencyLabel = UILabel()
    var textField = UITextField()
    var activityIndicator: UIActivityIndicatorView?
    
    var defaultKeys = ["USD", "EUR", "CAD", "GBP", "PLN"]
    var selectedArray: [CurrencyModel] = []
    var currencyArray: [CurrencyModel] = []
    var currentModel: CurrencyModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        currentModel = CurrencyModel(name: "PLN", value: 1.0, isSelected: true)
        
        setUpUpperView()
        setUpTableView()
        setUpConstraints()
        setUpActivityIndicator()
        mainTableView?.isHidden = true
        makeRequest()
    }
        
    func makeRequest() {
        DispatchQueue.global(qos: .background).async {
            guard let currentModel = self.currentModel else { return }
            NetworkManager.sharedInstance.makeRequest(currency: currentModel.name) { [weak self] (dict, error) in
                guard let self = self else { return }
                if let error = error {
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "ERROR", message: error.localizedDescription, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel))
                        self.present(alert, animated: true)
                        self.activityIndicator?.removeFromSuperview()
                    }
                }
                
                if let dictionary = dict {
                    dictionary.forEach { (key, value) in
                        let model = CurrencyModel(name: key, value: value, isSelected: false)
                        self.currencyArray.append(model)
                    }
                    self.currencyArray.forEach { (model) in
                        if self.defaultKeys.contains(model.name), model.name != currentModel.name {
                            self.selectedArray.append(model)
                            model.isSelected = true
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self.activityIndicator?.removeFromSuperview()
                        self.mainTableView?.reloadData()
                        self.mainTableView?.isHidden = false
                    }
                }
            }
        }
    }
    
    private func setUpUpperView() {
        clearView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(clearView)
        
        clearView.addSubview(currencyLabel)
        currencyLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview()
            make.width.equalTo(view.frame.width / 3)
            make.height.equalTo(view.frame.width * 0.15)
        }
        currencyLabel.textAlignment = .left
        currencyLabel.font = UIFont(name: "Helvetica Neue", size: 30)
        currencyLabel.text = currentModel?.name
        currencyLabel.layoutIfNeeded()
        
        clearView.addSubview(textField)
        textField.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview()
            make.width.equalTo(view.frame.width / 2)
            make.height.equalTo(view.frame.width * 0.15)
        }
        if let model = currentModel {
            textField.text = "\(model.value)"
        }
        textField.layoutIfNeeded()
        textField.font = UIFont(name: "Helvetica Neue", size: 30)
        textField.delegate = self
        textField.textAlignment = .right
        textField.returnKeyType = .done
        textField.keyboardType = .numbersAndPunctuation
        textField.borderStyle = .none
        textField.placeholder = "Enter amount"
    }
    
    private func setUpTableView() {
        mainTableView = UITableView()
        mainTableView?.dataSource = self
        mainTableView?.delegate = self
        mainTableView?.rowHeight = 78
        mainTableView?.register(CurrencyTableViewCell.self, forCellReuseIdentifier: "currencyCell")
        if let tableView = mainTableView {
            view.addSubview(tableView)
        }
    }
    
    private func setUpConstraints() {
        clearView.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor).isActive = true
        clearView.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor).isActive = true
        clearView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor).isActive = true
        clearView.heightAnchor.constraint(equalToConstant: view.frame.height * 0.15).isActive = true
        clearView.layoutIfNeeded()
        clearView.layoutSubviews()
        
        if let tableView = mainTableView {
            tableView.translatesAutoresizingMaskIntoConstraints = false
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
            tableView.topAnchor.constraint(equalTo: clearView.layoutMarginsGuide.bottomAnchor).isActive = true
            tableView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor).isActive = true
        }
    }
    
    func setUpActivityIndicator() {
        activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator?.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        view.addSubview(activityIndicator!)
        activityIndicator?.snp.makeConstraints({ (maker) in
            maker.centerX.equalToSuperview()
            maker.centerY.equalToSuperview()
        })
        activityIndicator?.startAnimating()
    }
}

