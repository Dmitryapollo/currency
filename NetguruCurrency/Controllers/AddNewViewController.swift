//
//  AddNewViewController.swift
//  NetguruCurrency
//
//  Created by Дмитрий on 29/10/2020.
//  Copyright © 2020 Дмитрий. All rights reserved.
//

import UIKit

protocol AddNewDelegate: class {
    func addnew(model: CurrencyModel)
}

class AddNewViewController: UIViewController {
    
    var tableView: UITableView?
    var array: [CurrencyModel] = []
    weak var delegate: AddNewDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpTableView()
    }
    
    private func setUpTableView() {
        tableView = UITableView()
        tableView?.dataSource = self
        tableView?.delegate = self
        tableView?.rowHeight = 78
        tableView?.register(CurrencyTableViewCell.self, forCellReuseIdentifier: "currencyCell")
        if let tableView = tableView {
            view.addSubview(tableView)
            
            tableView.translatesAutoresizingMaskIntoConstraints = false
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
            tableView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor).isActive = true
            tableView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor).isActive = true
        }
    }
}
