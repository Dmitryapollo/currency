//
//  ViewController+UITextField.swift
//  NetguruCurrency
//
//  Created by Дмитрий on 28/10/2020.
//  Copyright © 2020 Дмитрий. All rights reserved.
//

import UIKit

extension ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let set = NSCharacterSet(charactersIn:"0123456789.").inverted
        let compSepByCharInSet = string.components(separatedBy: set)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return string == numberFiltered
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        mainTableView?.reloadData()
        textField.resignFirstResponder()
        return true
    }
}
