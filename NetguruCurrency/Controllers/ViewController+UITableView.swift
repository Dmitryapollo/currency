//
//  ViewController+UITableView.swift
//  NetguruCurrency
//
//  Created by Дмитрий on 28/10/2020.
//  Copyright © 2020 Дмитрий. All rights reserved.
//

import UIKit

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == selectedArray.count {
            let vc = AddNewViewController()
            vc.array = currencyArray.filter({!$0.isSelected && $0.name != currentModel?.name})
            vc.delegate = self
            present(vc, animated: true)
            return
        }
        guard currentModel?.name != selectedArray[indexPath.row].name else { return }
        currentModel = selectedArray[indexPath.row]
        currencyLabel.text = currentModel?.name
        selectedArray = []
        currencyArray = []
        mainTableView?.isHidden = true
        setUpActivityIndicator()
        makeRequest()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Currency"
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "currencyCell", for: indexPath) as? CurrencyTableViewCell
        if indexPath.row == selectedArray.count {
            cell?.currencyLabel.text = "Add new"
            return cell ?? UITableViewCell()
        }
    
        let result = Float(selectedArray[indexPath.row].value) * ((textField.text as NSString?)?.floatValue ?? 1.0)
        cell?.currencyLabel.text = selectedArray[indexPath.row].name
        cell?.valueLabel.text = String(format: "%.03f", result)
    
        return cell ?? UITableViewCell()
    }
}

extension ViewController: AddNewDelegate {
    func addnew(model: CurrencyModel) {
        model.isSelected = true
        selectedArray.append(model)
        defaultKeys.append(model.name)
        mainTableView?.beginUpdates()
        mainTableView?.insertRows(at: [IndexPath(row: selectedArray.count, section: 0)], with: .automatic)
        mainTableView?.endUpdates()
        mainTableView?.reloadData()
    }
}
 
