//
//  AddNewViewController+UITableView.swift
//  NetguruCurrency
//
//  Created by Дмитрий on 29/10/2020.
//  Copyright © 2020 Дмитрий. All rights reserved.
//

import UIKit

extension AddNewViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "currencyCell", for: indexPath) as? CurrencyTableViewCell
        cell?.currencyLabel.text = array[indexPath.row].name
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = array[indexPath.row]
        delegate?.addnew(model: model)
        self.dismiss(animated: true)
    }
    
}
